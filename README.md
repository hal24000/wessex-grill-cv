# Computer Vision for Grill Detection

Ingestion, cropping, processing and computer vision for detcing water inlet/outlet blockages.

## Usage
```bash
$ cd grill-detect/src
$ streamlit run app.py
```

## Project Organisation

    ├── chart                        <- Files required for Dimensionops build
    │
    ├── notebooks                    <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                                   the creator's initials, and a short `-` delimited description, e.g.
    │                                   `1.0-KK-Initial-data-exploration`
    │
    ├── src                          <- Source code for project
    │   │
    │   │── model                    <- Model code
    │   │   │── model.py             <- Similarity calculation model
    │   │   │── plot.py              <- Visualization plots
    │   │   └── utils.py             <- Methods to store and collect data
    │   │
    │   │── pages                    <- App pages
    │   │   │── page_cv.py           <- Computer vision models
    │   │   │── page_dashboard.py    <- Main dashboard
    │   │   └── page_preprocess.py   <- Selected preprocessing pipeline
    │   │
    │   │── setup                    <- Basic setup files
    │   │   │── database.py          <- Database connection
    │   │   │── favicon.ico          <- Icon
    │   │   └── layout.py            <- Layout settings
    │   │
    │   └── app.py                   <- App run file
    │   
    │── Dockerfile                   <- File to assemble a Docker image
    │
    │── environment.yml              <- Environment yml file to create conda environment
    │
    ├── README.md                    <- README for this project
    │
    └── requirements.txt             <- Requirements file for creating app environment