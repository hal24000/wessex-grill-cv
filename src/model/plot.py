from typing import List, Tuple

import cv2
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plotly
import plotly.graph_objects as go
import scipy.signal
import streamlit as st

from model.utils import get_data
from setup.database import db_con


def plot_convolution(
    site_id: str, apply_offset: bool
) -> Tuple[matplotlib.figure.Figure, List[int]]:
    """
    Conducts convolution on two cropped images.
    If offset is applied, the image being studied will be offset.
    Then convolution performed again.
    """
    db = db_con()
    data, im1_orig = get_data(site_id, "Clear")
    _, im2_orig = get_data(site_id, "Blocked")
    im1_gray = cv2.cvtColor(im1_orig, cv2.COLOR_BGR2GRAY)
    im2_gray = cv2.cvtColor(im2_orig, cv2.COLOR_BGR2GRAY)

    x_ref_crop = slice(data["x_coords"][0], data["x_coords"][1])
    y_ref_crop = slice(data["y_coords"][0], data["y_coords"][1])

    if apply_offset:
        query = {
            "site_id": site_id,
            "site_state": "Blocked",
        }
        project = {"_id": 0, "offset_yx": 1}
        offset_yx = list(db["kk_cv_images"].find(query, project))[0]["offset_yx"]
        y_adjust, x_adjust = offset_yx[0], offset_yx[1]
    else:
        y_adjust, x_adjust = 0, 0

    x_crop = slice(data["x_coords"][0] + x_adjust, data["x_coords"][1] + x_adjust)
    y_crop = slice(data["y_coords"][0] + y_adjust, data["y_coords"][1] + y_adjust)

    im1_gray = im1_gray[y_ref_crop, x_ref_crop]
    im2_gray = im2_gray[y_crop, x_crop]

    corr_img_11 = scipy.signal.fftconvolve(im1_gray, im1_gray[::-1, ::-1], mode="same")
    corr_img_12 = scipy.signal.fftconvolve(im1_gray, im2_gray[::-1, ::-1], mode="same")

    shape_11 = np.unravel_index(np.argmax(corr_img_11), corr_img_11.shape)
    shape_12 = np.unravel_index(np.argmax(corr_img_12), corr_img_12.shape)

    # FIGURE
    fig, (ax1, ax2) = plt.subplots(1, 2)
    ax1.imshow(corr_img_11)
    ax2.imshow(corr_img_12)
    ax1.axis("off")
    ax2.axis("off")
    ax1.set_title(f"Ref/Ref Conv - {shape_11}")
    ax2.set_title(f"Ref/Blocked Conv {shape_12}")

    offset = [(a - b).item() for a, b in zip(shape_11, shape_12)]

    # STORE RESULT
    query = {
        "site_id": site_id,
        "site_state": "Blocked",
    }
    update = {"$set": {"offset_yx": offset}}
    db["kk_cv_images"].update(query, update)
    return fig, offset


def plot_cropped(site_id: str) -> matplotlib.figure.Figure:
    """Plots images of Clear/Blocked sites, cropped to Region of Interest."""
    data, im1_orig = get_data(site_id, "Clear")
    _, im2_orig = get_data(site_id, "Blocked")

    # CROP
    x_crop = slice(data["x_coords"][0], data["x_coords"][1])
    y_crop = slice(data["y_coords"][0], data["y_coords"][1])
    im1 = im1_orig[y_crop, x_crop]
    im2 = im2_orig[y_crop, x_crop]

    # FIGURE
    fig, (ax1, ax2) = plt.subplots(1, 2)
    ax1.imshow(im1)
    ax2.imshow(im2)
    ax1.axis("off")
    ax2.axis("off")
    ax1.set_title(f"Reference")
    ax2.set_title(f"Blocked")
    return fig


@st.cache(ttl=600)
def plot_marked_map(select_site: str, select_zoom: str) -> plotly.graph_objects.Figure:
    """
    Plots map with site locations and color based on risk.
    Score determined by Chan-Vese segmentation on the processed frame.
    """
    db = db_con()
    query = {"site_state": "Blocked"}
    project = {
        "_id": 0,
        "site_id": 1,
        "site_name": 1,
        "lat": 1,
        "lon": 1,
        "chan-vese": 1,
    }
    df = pd.DataFrame(db["kk_cv_images"].find(query, project))

    # SET RISK THRESHOLDS
    risk_list = []
    for i, row in df.iterrows():
        if row["chan-vese"] < 0.5:
            risk_list.append("High")
        elif (row["chan-vese"] > 0.5) & (row["chan-vese"] < 0.8):
            risk_list.append("Med")
        else:
            risk_list.append("Low")
    df["Risk"] = risk_list

    mapbox_access_token = open("setup/.mapbox_token").read()
    fig = go.Figure(
        go.Scattermapbox(
            name="",
            mode="markers",
            lat=df["lat"],
            lon=df["lon"],
            customdata=df[["site_name", "site_id", "Risk", "chan-vese"]],
            hovertemplate="""
        <b>Name</b>: %{customdata[0]} <br>
        <b>ID</b>: %{customdata[1]} <br>
        <b>Risk</b>: %{customdata[2]} <br>
        <b>Rating</b>: %{customdata[3]}
        """,
            marker=dict(
                size=10,
                color=df["Risk"]
                .map({"High": "red", "Med": "orange", "Low": "lightgreen"})
                .tolist(),
            ),
        )
    )
    fig.update_layout(
        legend_title_text="",
        legend=dict(
            x=0,
            y=0.10,
            traceorder="reversed",
            orientation="h",
            bgcolor="rgba(0,0,0,0)",
            font=dict(size=13),
        ),
        height=350,
        margin={"r": 10, "t": 10, "l": 10, "b": 10},
        mapbox_style="dark",
        hovermode="closest",
        mapbox=dict(
            accesstoken=mapbox_access_token,
            center=dict(
                lat=df[df["site_id"] == select_site]["lat"].values[0],
                lon=df[df["site_id"] == select_site]["lon"].values[0],
            ),
            zoom=select_zoom,
        ),
    )
    return fig
