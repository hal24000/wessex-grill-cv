from typing import Dict, Tuple

import cv2
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from skimage.metrics import structural_similarity
from skimage.segmentation import chan_vese

from model.utils import get_data, get_offset, name_to_id
from setup.database import db_con


def calculate_ssim(
    site_name: str, im1_state: str, im2_state: str, method: str, highlight: bool = True
) -> Tuple[np.float64, matplotlib.figure.Figure, matplotlib.figure.Figure]:
    """
    Pre-processes two images for computer vision
    - Offset correction
    - Crop
    - Grayscale
    - Normalize
    - Blurring and edge detection
    - Produces score
    - Generates figure which shows result when zoomed on ROI
    - Generates panned out figure
    - Note: Streamlit caching cannot be used
    """
    db = db_con()
    site_id = name_to_id(site_name)

    data, im1_orig = get_data(site_id, im1_state)
    _, im2_orig = get_data(site_id, im2_state)

    # GET OFFSET
    offset_yx = get_offset(site_id)
    y_adjust, x_adjust = offset_yx[0], offset_yx[1]

    # CROP
    x_ref_crop = slice(data["x_coords"][0], data["x_coords"][1])
    y_ref_crop = slice(data["y_coords"][0], data["y_coords"][1])
    x_crop = slice(data["x_coords"][0] + x_adjust, data["x_coords"][1] + x_adjust)
    y_crop = slice(data["y_coords"][0] + y_adjust, data["y_coords"][1] + y_adjust)
    im1 = im1_orig[y_ref_crop, x_ref_crop]
    im2 = im2_orig[y_crop, x_crop]

    # GRAYSCALE
    im1_gray = cv2.cvtColor(im1, cv2.COLOR_BGR2GRAY)
    im2_gray = cv2.cvtColor(im2, cv2.COLOR_BGR2GRAY)

    # NORMALIZE
    im1_gray_norm = cv2.normalize(
        im1_gray, None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX
    )
    im2_gray_norm = cv2.normalize(
        im2_gray, None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX
    )

    if method != "Original":
        # BLUR PREPROCESSING FOR EDGE DETECTION
        im1_gray_norm_blur = cv2.GaussianBlur(im1_gray_norm, (3, 3), 0)
        im2_gray_norm_blur = cv2.GaussianBlur(im2_gray_norm, (3, 3), 0)

        # EDGE DETECTION
        if method == "Canny":
            im1_edge = cv2.Canny(image=im1_gray_norm_blur, threshold1=0, threshold2=200)
            im2_edge = cv2.Canny(image=im2_gray_norm_blur, threshold1=0, threshold2=200)
        elif method == "Chan-Vese":
            im1_edge = chan_vese(
                im1_gray_norm_blur,
                mu=0.25,
                lambda1=1,
                lambda2=1,
                tol=1e-3,
                max_iter=200,
                dt=0.5,
                init_level_set="checkerboard",
                extended_output=True,
            )
            im2_edge = chan_vese(
                im2_gray_norm_blur,
                mu=0.25,
                lambda1=1,
                lambda2=1,
                tol=1e-3,
                max_iter=200,
                dt=0.5,
                init_level_set="checkerboard",
                extended_output=True,
            )
            # Final level set
            im1_edge = im1_edge[1]
            im2_edge = im2_edge[1]

        # FINAL
        im1_final = im1_edge.copy()
        im2_final = im2_edge.copy()
    else:
        im1_final = im1_gray_norm.copy()
        im2_final = im2_gray_norm.copy()

    # SCORE AND DIFFERENCE
    (score, diff) = structural_similarity(im1_final, im2_final, full=True)
    diff = (diff * 255).astype("uint8")

    # STORE RESULT
    query = {
        "site_id": site_id,
        "site_state": im2_state,
    }
    update = {"$set": {method.lower(): round(score, 2)}}
    db["kk_cv_images"].update(query, update)

    # THRESHOLD
    thresh = cv2.threshold(diff, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]

    # CONTOURS
    contours = cv2.findContours(
        thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
    )
    contours = contours[0] if len(contours) == 2 else contours[1]
    im_filled = im2.copy()
    if len(contours) > 0:
        contours_sorted = sorted(
            contours, key=lambda x: cv2.contourArea(x), reverse=True
        )
        for c in contours_sorted[:1]:
            # if cv2.contourArea(c)>90:
            im_filled = cv2.drawContours(
                image=im_filled,
                contours=[c],
                contourIdx=0,
                color=(0, 255, 255),
                thickness=-1,
            )
            alpha = 0.4
            result = cv2.addWeighted(im2, 1 - alpha, im_filled, alpha, 0)
    else:
        result = im_filled

    # FIGURE
    fig1, (ax1, ax2, ax3) = plt.subplots(1, 3)
    ax1.imshow(im1_final)
    ax2.imshow(im2_final)
    ax3.imshow(result)
    ax1.axis("off")
    ax2.axis("off")
    ax3.axis("off")
    ax1.set_title(f"{site_name} - {im1_state}", size=6)
    ax2.set_title(f"{site_name} - {im2_state}", size=6)
    ax3.set_title(f"{site_name} - Difference", size=6)

    im2_res = im2_orig.copy()
    y_offset = data["y_coords"][0]
    x_offset = data["x_coords"][0]
    im2_res[
        y_offset : y_offset + result.shape[0], x_offset : x_offset + result.shape[1]
    ] = result

    fig2, (ax4, ax5) = plt.subplots(1, 2)
    if highlight:
        ax4.imshow(im2_res)
    else:
        ax4.imshow(im2_orig)
    ax5.imshow(im1_orig)
    ax4.axis("off")
    ax5.axis("off")
    plt.tight_layout()
    return score, fig1, fig2
