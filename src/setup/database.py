import pymongo

# from dimensionops_helpers.server_request_handler import ServerRequestHandler
from pymongo import MongoClient


def db_con() -> pymongo.database.Database:
    """Connection to Mongo database."""
    db_uri = "mongodb://applic:Ka9zSNti4hYu5RC3pd0m@10.0.1.4:27017,10.0.1.5:27017,10.0.1.6:27017/?authSource=cd_metwessexcso&readPreference=primary&appname=MongoDB%20Compass&ssl=false"
    # test = ServerRequestHandler()
    # db_uri = test.get_mongo_secret()["mongoDbTenantSettings"][
    #     "tenantUserConnectionString"
    # ].replace("/?", "?")

    client = MongoClient(db_uri)
    db_con = client["cd_metwessexcso"]
    return db_con
